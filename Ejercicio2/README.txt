>> Ejercicio 1

Al ejecutar el programa se encontrará con un menu que ofrece 4 opciones, la opcion 1 permite ingresar un valor entero a la lista 1, la opcion 2 permite ingresar valores (int) a la lista 2, la opcion 3 permite visualizar todas las litas y la opcion 4 salir.
El programa solo recibe valores enteros para que sean ingresados a las listas.

    • REQUISITOS PREVIOS
     
Sistema operativo Linux 
GNU Make (para compilar) 


-- EJECUTANDO LAS PRUEBAS POR TERMINAL --
Para compilar el código se debe agregar el comando make por terminal, luego de realizar la compilación, se utiliza el comando ./programa para ejecutar.

    • CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.


    • VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

    • AUTORES

Valentina Garrido - Desarrollo del código, ejecución de proyecto y narración de README. 

    • EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.
