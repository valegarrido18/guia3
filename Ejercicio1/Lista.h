#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista {
    private:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;

    public:
        /* constructor*/
        Lista();
        
        /* crea un nuevo nodo */
        void crear (Numero *numero);
        /* imprime la lista. */
        void imprimir ();
};
#endif
