#include <iostream>
using namespace std;

/* definición de la estructura Nodo. */
#include "Ejemplo.h"

/* clases */
#include "Numero.h"
#include "Lista.h"


//Escriba un programa que solicite números enteros y los vaya ingresando a una lista enlazada simple
//ordenada. Por cada ingreso debe mostrar el estado de la lista.

class Ejemplo {
    private:
        Lista *lista = NULL;

    public:
        /* constructor */
        Ejemplo() {
            this->lista = new Lista();
        }
        
        Lista *get_lista() {
            return this->lista;
        }
};

/* función principal. */
int main (void) {
    Ejemplo e = Ejemplo();
    Lista *lista = e.get_lista();
    string a;
	bool x = true;
    while(x){
		cout << "ingresar numero ---> 1" << endl;
		cout << "salir           ---> 2" << endl;
		cin >> a;
		if (a == "1"){
			int b;			
			cout << "ingrese un numero para la lista: " << endl;
			cin >> b;
			lista->crear(new Numero(b));
			lista->imprimir();
		}
		else if (a == "2"){
			x =  false;
		}
		else {
			cout << "ingrese un dato valido" << endl;
		}
	}
    return 0;
}
